/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import org.json.JSONObject;


/**
 *
 * @author bharati
 */
class LogOut {
    private static org.apache.log4j.Logger log = Logger.getLogger(LogOut.class);
    String JDBC_DRIVER;  
    String DB_URL ;
    //  Database credentials
    String USER ;
    String PASS ;
    String urlString;
    Connection conn;
    Statement stmt;
    Map<String,String> paramMap;
    String myLoginToken =null;
    int responseCode =0;
    //JSONObject objCookie;
    JSONObject objError;
    public LogOut(){
        Context env;
        try {
            env = (Context)new InitialContext().lookup("java:comp/env");
            DB_URL = (String)env.lookup("DB_URL");
            //log.debug("DB_URL = " + DB_URL);
            JDBC_DRIVER = (String)env.lookup("JDBC_DRIVER");
            //log.debug("JDBC_DRIVER = " + JDBC_DRIVER);
            USER = (String)env.lookup("USER");
            //log.debug("USER = " + USER);
            PASS = (String)env.lookup("PASS");
            //log.debug("PASS = " + PASS);
            paramMap = new HashMap<String,String>() ;
            Utils myUtils = new Utils();
            boolean myb = myUtils.getParams(JDBC_DRIVER,DB_URL,USER,PASS );
            //log.debug("Got Params = " + myb);
            if(myb == true){
               paramMap = myUtils.paramMap;
            }
            
        } catch (NamingException ex) {
            java.util.logging.Logger.getLogger(LogOut.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
    }
    String logMeOutNow(String loginToken)
   {
        myLoginToken = loginToken;
        String result = ""; 
        OutputStreamWriter writer = null;
        BufferedReader reader = null;
        String urlParameters = null;
        StringBuilder postData = new StringBuilder();
        String postParameters = null;
        // Get the base naming context
        
        // Get a single value
        
        try {
            String myURLString = (String) paramMap.get("VCA_UMS_LOGOUT");
            log.debug("UMS URLString = "+ myURLString);
            URL obj = new URL(myURLString);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            log.debug("loginToken : "+ loginToken);
            String cookies =  loginToken ;
            log.debug("Cookie : "+ cookies);
            con.setRequestProperty("Cookie", cookies);
            con.setRequestProperty("X-XSRF-TOKEN", loginToken);
            //postParameters = new JSONObject().put("Set-Cookie", loginToken).toString();
            con.setDoOutput(true);
            PrintStream os = new PrintStream(con.getOutputStream());
            //os.print(postParameters);
            os.flush();
            os.close();
            
            responseCode = con.getResponseCode();
            log.debug("Response Code : " + responseCode);
            if ((responseCode/100 == 2) ||(responseCode/400 == 4)){
                log.debug("\nSending 'POST' request to URL : " + urlString);
                log.debug("Response Code : " + responseCode);
                //add code to get userinfo only if response code is 200
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
                }
                result += response.toString();
                log.debug("1.Result =" + result);
                String userIdStr = deleteFromLoginmap();
                log.debug("Token Id returned from delete ="+ userIdStr);
                con.disconnect();
                con = null;
                in.close();
            }
            else {
                result = "Bad Request";
            }
        }
        catch (MalformedURLException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        } catch (ProtocolException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        } catch (IOException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        }catch (Exception e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        }
    return  result;
   }
  
    
    String deleteFromLoginmap(){
        String tokenStr = "";
    try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
           stmt = conn.createStatement();
            String sql;
            log.debug("Lets delete from loginmap");
            String generatedColumns[] = {"token"};
            sql = "delete from login_map where token='"+myLoginToken+"';";
            log.debug("SQL:" + sql);
            int affectedRows = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            log.debug("Affected Rows = "+ affectedRows);
            ResultSet rs = stmt.getGeneratedKeys();
            log.debug("Generated Rs: "+ rs);
            if(rs != null && rs.next()){
                tokenStr = rs.getString(1);
            }
            log.debug("Generated tokenStr: "+ tokenStr);
      
            
        //STEP 6: Clean-up environment
        
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
   
        return tokenStr;
}
    
}
