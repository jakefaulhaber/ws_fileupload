/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bharati
 */
class Login {
    private static org.apache.log4j.Logger log = Logger.getLogger(Login.class);
    
    String JDBC_DRIVER;  
    String DB_URL ;
    //  Database credentials
    String USER ;
    String PASS ;
    String urlString;
    Connection conn;
    Statement stmt;
    String mylast;
    int i =0;
    //static Login _instance = null;
    String cookie = null;
    JSONObject objUser ; 
    JSONObject objCookie;
    JSONObject objVoiceowner;
    JSONObject objLanguage;
    JSONObject objError;
    Map<String,String> paramMap;
    JSONArray listLanguage;
    int responseCode = 0;
    String objCookieStr ="";
    String cookieRedis ="";
   /*private Login (){
       _instance = this;
   }
   static public Login getInstance(){
       if (_instance == null)
           _instance = new Login();
      
        return _instance;
   }
   */
    public Login(){
        Context env;
        try {
            env = (Context)new InitialContext().lookup("java:comp/env");
            DB_URL = (String)env.lookup("DB_URL");
            log.debug("DB_URL = " + DB_URL);
            JDBC_DRIVER = (String)env.lookup("JDBC_DRIVER");
            log.debug("JDBC_DRIVER = " + JDBC_DRIVER);
            USER = (String)env.lookup("USER");
            //log.debug("USER = " + USER);
            PASS = (String)env.lookup("PASS");
            //log.debug("PASS = " + PASS);
            paramMap = new HashMap<String,String>() ;
            //boolean myb = getParams();
            Utils myUtils = new Utils();
            boolean myb = myUtils.getParams(JDBC_DRIVER,DB_URL,USER,PASS );
            log.debug("Got Params = " + myb);
            if(myb == true){
               paramMap = myUtils.paramMap;
            }
        } catch (NamingException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
    }
    String authenticate(String email,
                       String password)
   {
        log.debug(email);
        log.debug(email);
        String result = ""; 
        OutputStreamWriter writer = null;
        BufferedReader reader = null;
        String urlParameters = null;
        StringBuilder postData = new StringBuilder();
        String postParameters = null;
        // Get the base naming context
        
        // Get a single value
        
        try {
            String myURLString = (String) paramMap.get("VCA_UMS");
            log.debug("UMS URLString = "+ myURLString);
            postParameters = new JSONObject().put("email_address", email).put("password", password).toString();
            URL obj = new URL(myURLString);
            int TIMEOUT_VALUE = 1000;
            HttpURLConnection con = null;
            try{
                con = (HttpURLConnection) obj.openConnection();
                con.setConnectTimeout(TIMEOUT_VALUE);
                con.setReadTimeout(TIMEOUT_VALUE);
                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "application/json");

                con.setDoOutput(true);
                log.debug("Connection Send");
                PrintStream os = new PrintStream(con.getOutputStream());
                os.print(postParameters);
                os.flush();
                os.close();
            }
            catch (java.net.SocketTimeoutException e)
                { log.debug( "request TImed out");
                 } 
            catch (java.io.IOException e)
                    { log.debug( "Request TImed out and the read failed." );
                    } 
            
            responseCode = con.getResponseCode();
            if (responseCode/100 == 2){
                log.debug("\nSending 'POST' request to URL : " + urlString);
                log.debug("Response Code : " + responseCode);
                //add code to get userinfo only if response code is 200
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
                }
                result = response.toString();
                objUser = new JSONObject(result);
                log.debug(result);
                String headerName=null;
                String cookie = null;
                String myStr[] = null;
                for (int i=1; (headerName = con.getHeaderFieldKey(i))!=null; i++) 
                {                       
                    if (headerName.equals("Set-Cookie")) {                  
                    cookie = con.getHeaderField(i);
                    //log.debug("cookie: " + cookie);
                    log.debug(cookie);
                    
                    
                    }
                   
                }
                //objCookie =new JSONObject().put("cookie",cookie);
                log.debug("created json"); 
                myStr = cookie.split(";");
                log.debug("myStr: " + myStr[0]);
                myStr[0].replace('=', ':');
                
                String cookie1 ="{" + myStr[0] +"}";
                log.debug("cookie1 = " + cookie1);
                objCookie =new JSONObject(cookie1);
                objCookieStr = myStr[0];
                in.close();
                con.disconnect();
                con = null;
                //add info to loginmap 
                long id  = objUser.getLong("id");
                int permissions = objUser.getInt("permissions");
                String str[] = null;
                str = myStr[0].split("=");
                log.debug("str: " + str[1]);
                log.debug("Connecting to database...");
                try{
                    //Class.forName("com.mysql.jdbc.Driver");
                    Class.forName(JDBC_DRIVER);
                    conn = DriverManager.getConnection(DB_URL,USER,PASS);
                    //STEP 4: Execute a query
                    log.debug("Creating statement...");
                    stmt = conn.createStatement();
                    String sql ;
                    sql = "insert into login_map(user_id,token,permissions,created,email_address) ";
                    sql += "values( "+id +", '" +str[1]+"', " + permissions + " , now(), '"+email+"');";
                    //log.debug("SQL: " + sql);
                    cookieRedis = str[1];
                    log.debug(sql);
                    int retv = stmt.executeUpdate(sql);
                    if (retv>0 )
                        log.debug("insert success");
                    else log.debug("insert failed");
                stmt.close();
                conn.close();
                }catch(SQLException se){
                //Handle errors for JDBC
                    //se.printStackTrace();
                    log.error("ERROR" + se.getLocalizedMessage());
                    log.debug("SQLState" + se.getSQLState());
                }catch(Exception e){
                //Handle errors for Class.forName
                    //e.printStackTrace();
                    log.error("ERROR" + e.getLocalizedMessage());
                }finally{
                 //finally block used to close resources
                try{
                    if(stmt!=null)
                    stmt.close();
                    }catch(SQLException se2){
                        log.error("ERROR" + se2.getLocalizedMessage());
                }// nothing we can do
                try{
                    if(conn!=null)
                        conn.close();
                    }catch(SQLException se){
                        se.printStackTrace();
                        log.error("ERROR " + se.getLocalizedMessage());
                    }//end finally try
                }//end try
                
            }
            else if (responseCode/100 == 4){
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                inputLine = in.readLine();
                log.debug("1.line ="+inputLine+"\n");
                try{
                    objError = new JSONObject(inputLine);
                    if (objError == null)
                        log.debug("1.error obj = null"  );
                    else
                        log.debug("1.1.error obj =" + objError.toString());
                }
                catch(JSONException e){
                    log.debug("Error:" + e.getMessage());
                }
                catch (Throwable ex){
                    log.debug("Throwable:" + ex.getMessage());
                }
                /*while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
                log.debug("2.line ="+inputLine+"\n");
                }*/
                //result = response.toString();
                //objError = new JSONObject(result);
                //log.debug("result);
                result ="error";
                con.disconnect();
            }
        
        } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        } catch (MalformedURLException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        } catch (ProtocolException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        } catch (IOException e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        }catch (Exception e) {
                e.printStackTrace();
                log.error("ERROR " +e.getLocalizedMessage());
        }
    return  result;
}
    
    int getLanguage()
   {    long id =0;
        String name;
        String region;
        boolean is_rtl;
        listLanguage = new JSONArray();
        try{
        //STEP 2: Register JDBC driver
            try{
                Class.forName(JDBC_DRIVER);
            }
            catch (ClassNotFoundException e ){
                log.debug("Where is the PostgreSQL JDBC Driver. Include in libpath");
            }
            log.debug("PostgreSQl Driver Registered");
        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            long idLong = objUser.getLong("id");
            sql = "SELECT is_rtl,language_id,language,region FROM language ;";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            log.debug(sql);
        //STEP 5: Extract data from result set
            
        while(rs.next()){
         //Retrieve by column name
            id  = rs.getLong("language_id");
            name = rs.getString("language");
            region = rs.getString("region");
            is_rtl = rs.getBoolean("is_rtl");
            if(region == null){
                objLanguage = new JSONObject().put("language_id",id )
                        .put("language", name).put("is_rtl", is_rtl);
            }
            else {
                objLanguage = new JSONObject().put("language_id",id ).put("language", name).put("region",region).put("is_rtl", is_rtl);
            }
            log.debug(objLanguage.toString());
            listLanguage.put(objLanguage);
        }
            //Display values
         
         
            
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.error("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.error("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.error("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.error("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
       
       
       return listLanguage.length();
   
   }
    long createMyVoiceOwner(JSONObject objUser){
     long voice_owner_id =0;
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
           stmt = conn.createStatement();
            String sql;
            String generatedColumns[] = {"voice_owner_id"};
            sql = "INSERT INTO voice_owner(" +
            "first_name, last_name," +
            "email_address, user_id)VALUES ('"+ 
            objUser.get("first_name") +"' , '"+ 
            objUser.get("last_name") +"' , '" +
            objUser.get("email_address") +"' ," + objUser.get("id")+ ");";
            log.debug("SQL:" + sql);
            int affectedRows = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            log.debug(sql);
            //STEP 5: Extract data from result set

            log.debug("Affected Rows = "+ affectedRows);
            ResultSet rs = stmt.getGeneratedKeys();
            log.debug("Generated Rs: "+ rs);
            if(rs != null && rs.next()){
                voice_owner_id = rs.getLong(1);
             }
            log.debug("Generated Voice Owner Id: "+ voice_owner_id);
            //Display values
            
            objVoiceowner = new JSONObject().put("voice_owner_id",voice_owner_id );
            log.debug(objVoiceowner.toString());
            
        //STEP 6: Clean-up environment
        
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
   
        return voice_owner_id;
        
    }
    long getVoiceOwner()
   {    long id =0;
        try{
        //STEP 2: Register JDBC driver
            try{
                Class.forName(JDBC_DRIVER);
            }
            catch (ClassNotFoundException e ){
                log.debug("Where is the PostgreSQL JDBC Driver. Include in libpath");
            }
            log.debug("PostgreSQl Driver Registered");
        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            long idUserLong = objUser.getLong("id");
            String myEmail = objUser.getString("email_address");
            sql = "SELECT voice_owner_id,user_id FROM voice_owner where user_id ="+ idUserLong + " OR email_address='"+myEmail+"';";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            log.debug(sql);
        //STEP 5: Extract data from result set
        long uid = 0;    
        while(rs.next()){
         //Retrieve by column name
            id  = rs.getLong("voice_owner_id");
            uid = rs.getLong("user_id");
        }
            //Display values
         objVoiceowner = new JSONObject().put("voice_owner_id",id );
         log.debug(objVoiceowner.toString());
         if (uid == 0) {
             //update table to  have user_id
             updateWithUserId(id, idUserLong);
         }  
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.error("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.error("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.error("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.error("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
       
       
       return id;
   
   }
    int updateWithUserId(long voice_owner_id, long user_id){
       
        int affectedRows =0;
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
           stmt = conn.createStatement();
            String sql;
            String generatedColumns[] = {"voice_owner_id"};
            sql = "Update voice_owner set user_id = " + user_id;
            sql += " where voice_owner_id = "+ voice_owner_id +"  ;";
            log.debug("SQL:" + sql);
            affectedRows = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            log.debug(sql);
            //STEP 5: Extract data from result set

            log.debug("Affected Rows = "+ affectedRows);
            ResultSet rs = stmt.getGeneratedKeys();
            log.debug("Generated Rs: "+ rs);
            if(rs != null && rs.next()){
                voice_owner_id = rs.getLong(1);
             }
            log.debug("Generated Voice Owner Id: "+ voice_owner_id);
            //Display values
            
            
        //STEP 6: Clean-up environment
        
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
   
        return affectedRows;
   }
}
