/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;


import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author bharati
 */
@Path("login")
public class LoginResource {

    @Context
    private UriInfo context;
    @Context 
    private HttpHeaders headers;
    private static final org.apache.log4j.Logger log = Logger.getLogger(LoginResource.class);
    
    /**
     * Creates a new instance of LoginResource
     */
    public LoginResource() {
    }

    /**
     * Retrieves representation of an instance of com.smi.LoginResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
        //TODO return proper representation object
         return "<h1>Let us authenticate Users!!</h1>";
    }

    /**
     * PUT method for updating or creating an instance of LoginResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response postHandler(final String inputJsonStr) throws JSONException {
        
        //log.debug("I will start to get object " + inputJsonStr);
        Version v = new Version();
        log.debug("VERSION = " + v.getVersion());
        log.warn("VERSION = " + v.getVersion());
        log.error("VERSION = " + v.getVersion());
        String email = null;
        String password = null;
        String result="";
        Login login = null;
        JSONObject myContent = null;
        JSONObject myObjects = null;
        JSONObject inputJsonObj = null;
        try {
            inputJsonObj = new JSONObject(inputJsonStr);
        } catch (JSONException ex) {
            log.debug("Error: " +ex.getMessage() +" " +ex.getLocalizedMessage());
        }
        int responseCode =0; 
        Response.ResponseBuilder builder;
        if (inputJsonObj.has("email")){
            try {
                email = (String) inputJsonObj.getString("email");
            }
            catch (Exception e){
                //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                result += "incorrect value for email.";
            }
        }
        else{
            result += "missing parameter email.";

        }
        if (inputJsonObj.has("password")){
            try {
                password = (String) inputJsonObj.getString("password");
            }
            catch (Exception e){
                //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                result += "incorrect value for password.";
            }
        }
        else{
            result += "missing parameter password.";

        }
        
        int len = result.length();
        log.debug( "Result length = " + len);
        if(result.length() == 0)
        {
            //log.debug("got userinfo email="+ email+" password="+password);
            if ((email == null) ||(email.equals(""))||(email.length()==0)||(email.isEmpty()))
                result += "The email cannot be blank.";
            if ((password==null) || (password.equals(""))||(password.length()==0)||(password.isEmpty()) )
                result += " The password cannot be blank.";
        }
        len = result.length();
        log.debug( "Result length = " + len);
        if(result.length() == 0){
            login = new Login();
            String rs = login.authenticate(email, password);
            log.debug("rs = " + rs);
            log.debug("login.responseCode = "+ login.responseCode);
            if((login.responseCode/100) == 2){
                int count = login.getLanguage();
                try{
                    JSONObject userobj = login.objUser;
                    long retval = login.getVoiceOwner();
                    if( retval == 0){
                        //create voice owner id
                        retval = login.createMyVoiceOwner( userobj);
                    }
                    log.debug("login voiceOwner = "+ login.objVoiceowner.toString());
                    log.debug("got VoiceOWner ");
                    JSONObject voiceowner = login.objVoiceowner;
                    log.debug ("Voice Owner = "+ voiceowner);
                    userobj.put("voice_owner_id", ""+retval);
                    myContent = new JSONObject().put("user", userobj);
                            //.put("language", login.listLanguage);
                    UtilRedis utilRedis = new UtilRedis();
                    long id = userobj.getLong("id");
                    log.debug("Now lets see if id = "+ id+ " exists in Redis ");
                    boolean foundInRedis = utilRedis.checkRedis(id, login.cookieRedis);
                    if(!foundInRedis){
                        //add object to redis

                        boolean objCreated;
                        objCreated = utilRedis.createUser(id, userobj, login.listLanguage,  login.cookieRedis);
                        if(objCreated ==true){
                            log.debug("Added Entry for " + userobj.toString()+" created in redis!");
                        }
                        else{
                            log.debug("Added Entry for " + userobj.toString()+" not created in redis!");
                        }
                    }
                    
                }
                catch(Exception e){
                //Handle errors for Class.forName
                    e.printStackTrace();
                    log.debug("error : "+e.getMessage());
                    }
                //Login.listLanguage  = null;
                }
                else{
                    try{
                        //responseCode = login.responseCode;
                        responseCode = 200;
                         myContent = new JSONObject().put("error message",login.objError.toString());
                         log.debug("error : "+myContent.toString());
                        }
                    catch(Exception e){
                        //Handle errors for Class.forName
                        e.printStackTrace();
                        log.debug("error : "+e.getMessage());
                    }
                }
        log.debug("print the login object");
        log.debug(myContent.toString());
        
        //return myContent;
        myObjects =  new JSONObject().put("results", myContent);
        result = "" + myObjects;
        }
        
        if (login != null)
        {
            int a =200;
            builder= Response.status(a);
            responseCode = login.responseCode;
        }
        else{
            int a =200;
            responseCode = 200;
            builder= Response.status(a);
        }
        
        if((responseCode/100) == 2){
            builder.header("Content-Type","application/json");
            log.debug("Before set cookie = " + login.objCookie.toString() );
            String cookieStr = login.objCookieStr + ";path=/";
            log.debug("After Adding Path = " + cookieStr );
            builder.header("Set-Cookie",cookieStr);
            builder.entity(result);
        }
        else if (((responseCode/100) == 4)||((responseCode/100) == 5)){
            builder.header("Content-Type","application/json");
            builder.header("Set-Cookie","");
            log.debug("Before sending response = " + result );
            //result = "{Error: " +result +"}";
            //myContent = new JSONObject().put("Error Code", 1).put("Error Message",result);
            //myObjects =  new JSONObject().put("Results", myContent);
            result = "" + myObjects;
            builder.entity(result);
        }
        
        return builder.build();
    }
}
