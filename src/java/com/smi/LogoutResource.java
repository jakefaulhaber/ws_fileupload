/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author bharati
 */
@Path("logout")
public class LogoutResource {

    @Context
    private UriInfo context;
    @Context 
    private HttpHeaders headers;
    private static final org.apache.log4j.Logger log = Logger.getLogger(LogoutResource.class);
    

    /**
     * Creates a new instance of LogoutResource
     */
    public LogoutResource() {
    }

    /**
     * Retrieves representation of an instance of com.smi.LogoutResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/html")
    public String getHtml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of LogoutResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response postHandler(final String inputJsonStr) throws JSONException {
        log.debug("I will start to get object " + inputJsonStr);
        Version v = new Version();       
        log.debug("VERSION = " + v.getVersion());    
        log.warn("VERSION = " + v.getVersion());      
        log.error("VERSION = " + v.getVersion());
        //log.debug("Input String = "+ inputJsonStr);
        String myContent = "Headers are";
        String loginToken = null;
        String result="";
        int responseCode =0;
        JSONObject myObjects = null;
        JSONObject inputJsonObj = null;
        long trss_id =0;
        try {
            inputJsonObj = new JSONObject(inputJsonStr);
        } catch (JSONException ex) {
            log.debug("Error: " +ex.getMessage() +" " +ex.getLocalizedMessage());
        }
        if (inputJsonObj.has("trss_id")){
        try {
            trss_id = (long) inputJsonObj.getLong("trss_id");
        }
        catch (Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("Error : "+e.getMessage());
            result += "incorrect value for trss_id.";
        }
        }
        else{
             result += "missing parameter trss_id.";
        }
        if(result.length()== 0){
        if (headers != null) {
            for (String header : headers.getRequestHeaders().keySet()) {
                myContent = myContent + "Header:"+ header+" Value:"+ headers.getRequestHeader(header)+"\n";
                if (header.equalsIgnoreCase("Cookie"))
                    loginToken = ""+  headers.getRequestHeader(header);
                //log.debug("HeaderContent = " + myContent);
                //log.debug("loginToken = " + loginToken);
            }
        }
        log.debug("check for login token");
        Utils myUtils = new Utils();
        UtilRedis utilRedis = new UtilRedis();
        LogOut logMeOut = new LogOut();
        boolean tokenFound = utilRedis.authenticateLoginToken(loginToken);
        log.debug("Token Found = "+ tokenFound );
        if (tokenFound ==true ){
            result = logMeOut.logMeOutNow(loginToken);
            log.debug("String from Logout = " +result);
            responseCode = logMeOut.responseCode;
            if(responseCode == 200){
               
                boolean objDeleted = utilRedis.removeUser(loginToken);
                
                if(objDeleted){
                    log.debug("The user object is deleted from redis");
                }
                else{
                    log.debug("The user object is not deleted from redis user:" + myUtils.loginToken );
                }
            }
        }
        else{
            responseCode = 401;
            String errortext= "unauthorized access.";
            if (tokenFound == false)
                errortext +=" login token is not valid.";
               myObjects =new JSONObject().put("status code", responseCode).put("error message",  errortext);
               responseCode = 200;
            }
        }
        else{
            responseCode = 200;
            String errortext = result;
            myObjects =new JSONObject().put("error message",  errortext);
        }
        if(responseCode/100 ==2){
                result = "logout successful";
                //JSONObject resultNew = new JSONObject().put("status_code", result );
                myObjects = new JSONObject().put("success message", result);
        }
        if(responseCode/100 ==4){
                myObjects = new JSONObject().put("status code", responseCode).put("error message",  result);
                responseCode = 200;
        }
        
         
        JSONObject myO = new JSONObject().put("results", myObjects);
        result = "" + myO;
        log.debug("Result before sending = "+ result);
        Response.ResponseBuilder builder= Response.status(responseCode);
        if((responseCode/100) == 2){
            builder.header("Content-Type","application/json");
            builder.entity(result);
        }
        else if ((responseCode/100) == 4||(responseCode/100) == 5){
            builder.header("Content-Type","application/json");
            builder.entity(result);
        }
        
        return builder.build();
    }
}
