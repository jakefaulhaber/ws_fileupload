/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;

/**
 *
 * @author bharati
 */
public class Myfile {
    String JDBC_DRIVER;  
    String DB_URL ;
    //  Database credentials
    String USER ;
    String PASS ;
   String urlString;
   Connection conn;
   Statement stmt;
    Map<String,String> paramMap;
    String loginToken;
    String sessionToken;
    private static org.apache.log4j.Logger log = Logger.getLogger(Myfile.class);
    public Myfile(){
        Context env;
        try {
            env = (Context)new InitialContext().lookup("java:comp/env");
            DB_URL = (String)env.lookup("DB_URL");
            //log.debug("DB_URL = " + DB_URL);
            JDBC_DRIVER = (String)env.lookup("JDBC_DRIVER");
            //log.debug("JDBC_DRIVER = " + JDBC_DRIVER);
            USER = (String)env.lookup("USER");
            //log.debug("USER = " + USER);
            PASS = (String)env.lookup("PASS");
            //log.debug("PASS = " + PASS);
            //urlString = (String)env.lookup("umsurl");
            //log.debug("urlString = "+ urlString );
            paramMap = new HashMap<String,String>() ;
            boolean myb = getParams();
            log.debug ("paramMap = " + paramMap.toString());
            //log.debug("Got Params = " + myb);
            
        } catch (NamingException ex) {
            java.util.logging.Logger.getLogger(Myfile.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
    }
    boolean validateLoginToken(String myloginToken, long voice_id){
        log.debug("Now in the validateLoginToken method");
       boolean  tokeninDB = false;
       String droptable = "drop table";
       boolean found = myloginToken.contains(droptable);
       if (found == true )
           return tokeninDB;
        if(!myloginToken.contains(";")){
           log.debug("loginTokenStr = " + myloginToken + "Does not contain ';'. The header is of wrong format");
           return tokeninDB;
       }
       String[] parts = myloginToken.split(";");
       if (parts.length != 2)
          return tokeninDB;
       log.debug("2.login Token = "+ parts[0]);
       log.debug("2.session Token = "+ parts[1]);
       String loginStr = parts[0];
       String sessionStr = parts[1];
       if(!loginStr.contains("="))
           return tokeninDB;
       String[] loginparts = loginStr.split("=");
       if (loginparts.length != 2)
          return tokeninDB;
       if (loginparts[1] ==null)
           return tokeninDB;
       if (loginparts[1].length() == 0 )
           return tokeninDB;
       String loginTokenStr = loginparts[1];
       
       loginTokenStr = loginTokenStr.replace("\"", "");
       log.debug("loginTokenStr = " + loginTokenStr);
       if(!sessionStr.contains("="))
           return tokeninDB;
       String[] sessionparts = sessionStr.split("=");
       if (sessionparts.length != 2)
          return tokeninDB;
       if (sessionparts[1] ==null)
           return tokeninDB;
       if (sessionparts[1].length() == 0 )
           return tokeninDB;
       String sessionTokenStr = sessionparts[1];
       if(!sessionTokenStr.contains("]"))
           return tokeninDB;
       sessionparts = sessionTokenStr.split("]");
       if (sessionparts.length != 1)
          return tokeninDB;
       sessionTokenStr = sessionparts[0];
       sessionTokenStr = sessionTokenStr.replace("\"", "");
       log.debug("sessionTokenStr = " + sessionTokenStr);
       loginToken = loginTokenStr;
       sessionToken = sessionTokenStr;
       log.debug("3.login Token = "+ loginToken);
       log.debug("3.session Token = "+ sessionToken);
       boolean foundLogin = false;
       boolean foundSession = false;
       try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT token FROM login_map where token ='"+ loginTokenStr + "';";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            String name = rs.getString("token");
            //log.debug("token = " +name);
            if (name.equals(loginTokenStr))
            {   foundLogin =true;
                log.debug("token in DB = "+name );
            }
            else{   foundLogin =false;
                   // log.debug("token not in DB = " );
            }
        }
        
            sql = "SELECT uid,session_id FROM session where voice_id ='"+ voice_id + "' and uid ='"+sessionTokenStr +"';";
            log.debug("SQL:" + sql);
            ResultSet rs1 = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs1.next()){
         //Retrieve by column name
            
            String uid = rs1.getString("uid");
            //log.debug("uid = " +uid);
            if (uid.equals(sessionTokenStr))
            {   foundSession =true;
                log.debug("session in DB = "+uid );
                break;
            }
            else{   foundSession =false;
                    //log.debug("session not in DB = " );
            }
        }
            //Display values
        //STEP 6: Clean-up environment
        rs1.close();
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
       tokeninDB = (foundLogin == true) && ( foundSession == true);
       return tokeninDB;
       
   }
    boolean getParams(){
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
        log.debug("Creating statement...");
        stmt = conn.createStatement();
        String sql;
        ResultSet rs;
        String name;
        long longval;
        String textval;
        double doubleval;
        boolean booleanval;
        java.sql.Timestamp ts;
        int intval;
        int configuration_params_type_id = 0;
        sql = "SELECT * FROM configuration_params where language_id =0 and domain = 'default' ";
            log.debug("SQL:" + sql);
            rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
           name = rs.getString("name");
           configuration_params_type_id = rs.getInt("configuration_params_type_id");
           switch(configuration_params_type_id){
               case 1: {
                        textval = rs.getString("val_text");
                        paramMap.put(name,textval);
                        break;
                    }
               case 2: {
                        longval = rs.getLong("val_bigint");
                        paramMap.put(name,String.valueOf(longval));
                        break;
                    }
               case 3: {
                        doubleval = rs.getDouble("val_numeric");
                        paramMap.put(name,String.valueOf(doubleval));
                        break;
                    }
               case 4: {
                        booleanval = rs.getBoolean("val_bool");
                        paramMap.put(name,String.valueOf(booleanval));
                        break;
                    }
               case 5: {
                        ts = rs.getTimestamp("val_date");
                        paramMap.put(name,ts.toString());
                        break;
                    }
               case 6: {
                        intval = rs.getInt("val_integer");
                        paramMap.put(name,String.valueOf(intval));
                        break;
                    }
           }
           
            
        }
        
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        boolean val;
        val = paramMap.size()>0;
    return val;
    }
    
    String createMyOVC(long voiceid,
            long textid,long sessionid,
            String filename, int samplerate, int bitdepth, int numchannels,
             int loc, String name, String notes, int voice_clip_status_id, 
             String email, String ovc_short_file_name, double clip_play_time
            ){
     long ovc_id = 0;
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
           stmt = conn.createStatement();
            String sql;
            log.debug("Lets insert ovc details");
            String generatedColumns[] = {"ovc_id"};
            /*
            sql = "insert into ovc(training_text_id, session_id,"
            + "sample_rate, bit_depth, num_channels, voice_id, level_of_confidence," 
            +"device_id, recorded_time, " 
            +"noise_level, volume_level, file_name, noise_data)" 
            +"VALUES (" +textid+" , "+sessionid + " , "+ samplerate +" , "+bitdepth+" , "+numchannels +" , "+ voiceid+" , "+loc+" , "
            + deviceid +" ,now(), " + noiselevel+" , "
            + volumelevel +" , '"+filename +"', '"+noisedata  + "');";
                    */
            
            sql = "insert into ovc( training_text_id, talent_recording_session_id, sample_rate, ";
            sql += " bit_depth, num_channels, voice_id, level_of_confidence,";
            sql += " recorded_time, file_name, name, notes, voice_clip_status_id, uploader_name, ";
            sql += " ovc_short_file_name, clip_play_time  ) VALUES  ";
            sql +="(" + textid +" , "+ sessionid + " , "+ samplerate +" , "+ bitdepth +" , "+ numchannels ;
            sql += " , "+ voiceid +" , "+ loc + " ,now(), '" + filename +"', '"+name  + "', '";
            sql +=  notes +"', " + voice_clip_status_id + ",'"+ email +"', '" ;
            sql += ovc_short_file_name +"', "+ clip_play_time+ ");";
            log.debug("SQL:" + sql);
            int affectedRows = stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            log.debug("Affected Rows = "+ affectedRows);
            ResultSet rs = stmt.getGeneratedKeys();
            log.debug("Generated Rs: "+ rs);
            if(rs != null && rs.next()){
                ovc_id = rs.getLong(1);
            }
            log.debug("Generated ovc Id: "+ ovc_id);
      
            
        //STEP 6: Clean-up environment
        
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                //se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
   
        
        
        String myVal ="" + ovc_id;
        return myVal;
    
}
    boolean voiceIdBelongsToUser(long voiceId,String loginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceIdBelongsToUser");
    boolean  voiceinDB = false;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select voice_id from voice where voice_owner_id = (select voice_owner_id from voice_owner where user_id =(select user_id from login_map where token='"+ loginToken + "'));";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myVoiceId = rs.getLong("voice_id");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (myVoiceId == voiceId)
            {   voiceinDB =true;
                log.debug("MyVoiceId in DB = "+myVoiceId );
                break;
            }
            else{   voiceinDB =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return voiceinDB;
}
    boolean sessionIdBelongsToUser(long session_id, String myloginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method sessionIdBelongsToUser");
    boolean  sessioninDB = false;
    String[] parts = myloginToken.split(";");
       String loginStr = parts[0];
       String sessionStr = parts[1];
       String[] loginparts = loginStr.split("=");
       String loginTokenStr = loginparts[1];
       loginparts = loginTokenStr.split("]");
       loginTokenStr = loginparts[0];
       loginTokenStr = loginTokenStr.replace("\"", "");
       log.debug("loginTokenStr = " + loginTokenStr);
       String[] sessionparts = sessionStr.split("=");
       String sessionTokenStr = sessionparts[1];
       sessionparts = sessionTokenStr.split("]");
       sessionTokenStr = sessionparts[0];
       sessionTokenStr = sessionTokenStr.replace("\"", "");
       loginToken = loginTokenStr;
       sessionToken = sessionTokenStr;
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select session_id from session where uid = '" +sessionTokenStr +"';";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long mySessionId = rs.getLong("session_id");
            //log.debug("MyvoiceOwnerId = " + mySessionId);
            if (mySessionId == session_id)
            {   sessioninDB =true;
                log.debug("MyvoiceOwnerId in DB = "+mySessionId );
                break;
            }
            else{   sessioninDB =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return sessioninDB;
}
    boolean deviceIDinDB(long deviceId,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method verify device id");
    //log.debug("Login Token = " + loginToken);
    boolean  deviceinDB = false;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select count(*) as myCount from device where device_id = "+ deviceId + ";";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long count = rs.getLong("myCount");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (count>0)
            {   deviceinDB =true;
                log.debug("Count of device_id in DB = "+count );
                break;
            }
            else{   deviceinDB =false;
                    log.debug("device id not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return deviceinDB;
}
    boolean textIDinDB(long textId,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method verify device id");
    //log.debug("Login Token = " + loginToken);
    boolean  textIdinDB = false;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select count(*) as myCount from training_text where training_text_id = "+ textId + ";";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long count = rs.getLong("myCount");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (count>0)
            {   textIdinDB =true;
                log.debug("Count of text_id in DB = "+count );
                break;
            }
            else{   textIdinDB =false;
                    log.debug("text id not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return textIdinDB;
}

    String getLangauageName(long sessionId) {
        String name = "";
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT lang.name as name";
            sql +=" FROM talent_recording_session trs " ;
            sql += " INNER JOIN voice v ON (reference_voice_id = v.voice_id) ";
            sql += " INNER JOIN language lang USING (language_id) ";
            sql +=" WHERE visible = true ";
            sql +=" and talent_recording_session_id = "+ sessionId;
            sql += " ORDER by talent_recording_session_name";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
            
        while(rs.next()){
         //Retrieve by column name
            
            name = rs.getString("name");
            
        }
         //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        return  name;
    }

    String getTalentType(long sessionId, long voiceId)  {
        String name = "";
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT   v.type as name  FROM talent_recording_session_state ";
            sql +=" INNER JOIN voice v ON (voice_owner_id = v.voice_owner_id)";
            sql +=" where talent_recording_session_id = " +sessionId;
            sql += " and voice_id = "+ voiceId;
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
            
        while(rs.next()){
         //Retrieve by column name
            
            name = rs.getString("name");
            
        }
         //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        return  name;
        
    }

    String getRecordingSessionName(long sessionId) {
        String name = "";
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT  talent_recording_session_name as name ";
            sql += " FROM talent_recording_session where talent_recording_session_id = " ;
            sql += sessionId;
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
            
        while(rs.next()){
         //Retrieve by column name
            
            name = rs.getString("name");
            
        }
         //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        return  name;
    
    }

    String getVoiceName(long voiceId) {
        String name = "";
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT  name FROM voice where voice_id = " ;
            sql += voiceId;
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
            
        while(rs.next()){
         //Retrieve by column name
            
            name = rs.getString("name");
            
        }
         //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        return  name;
    }
    
    
}
