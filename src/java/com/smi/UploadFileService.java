/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;

/**
 *
 * @author bharati
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/file")
public class UploadFileService {
    @Context 
    private HttpHeaders headers;
    private static org.apache.log4j.Logger log = Logger.getLogger(UploadFileService.class);
    private static final String UPLOAD_LOCATION_FOLDER = "G://upload/";
   
    
    @GET
    @Produces("text/html")
    public String getHtml() {
        //TODO return proper representation object
        return "<h1> Upload here </h1>";
    }

    /**
     * PUT method for updating or creating an instance of CompareclipsResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }
    
    
    @POST
	@Path("/upload")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
        @FormDataParam("file") InputStream uploadedInputStream,
        @FormDataParam("file") FormDataContentDisposition fileDetail,
        @FormDataParam("voice_id") String voiceIdS,
        @FormDataParam("training_text_id") String textIdS,
        @FormDataParam("talent_recording_session_id") String sessionIdS,
        @FormDataParam("num_channels") String numchannelsS,
        @FormDataParam("sample_rate") String samplerateS,
        @FormDataParam("bit_depth") String bitdepthS,
        @FormDataParam("level_of_confidence") String locS,
        @FormDataParam("notes")	String notesS,
        @FormDataParam("voice_clip_status_id") String voiceClipStatusIdS,
        @FormDataParam("clip_play_time")  String clipPlayTimeS,
        @FormDataParam("short_file_name")  String shortFileNameS
        ) {
        String output="";
        JSONObject myObjects = null;
        int countParams =0;
        int goodParams =0;
        int count[] = {8000, 16000,24000,32000,44100,48000};
        Set<Integer> sampleRateSet = new HashSet<Integer>();
        try{
            for(int i = 0; i<6; i++){
                sampleRateSet.add(count[i]);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            log.debug("Error : "+e.getMessage());
        }

        String filename = fileDetail.getFileName();
        log.debug("file = "+ filename);
        String ext = null;
        int i = filename.lastIndexOf('.');
        if (i >= 0) {
            ext = filename.substring(i+1);
        }
        if ((ext.compareToIgnoreCase("exe")==0)||(ext.compareToIgnoreCase("jpg")==0))
        {
            log.debug("file = "+ filename +"cannot be uploaded");
            output = "Error: File could not be uploaded. You can upload audio files only." ;
            return Response.status(401).entity(output).build();
        }
        else
        {   log.debug("DUMP STRINGS ");
            log.debug("voiceId = " + voiceIdS);
            log.debug("textId = " + textIdS);
            log.debug("sessionId = " + sessionIdS);
            //log.debug("deviceID = " +deviceIdS);
            log.debug("num channels = "+numchannelsS);
            log.debug("samplerate = "+samplerateS);
            log.debug("bitdepth = "+ bitdepthS);
            //log.debug("noiseleve = "+ noiselevelS);
            //log.debug("VolumeLevel = "+ volumelevelS);
            log.debug("loc = "+ locS);
            log.debug("notes = " + notesS);
            log.debug("voice_clip_status_id = " + voiceClipStatusIdS);
            log.debug("clip_play_time = " + clipPlayTimeS);
            log.debug("short_file_name = " + shortFileNameS);
            log.debug("Now Convert Datatypes! ");
            long voiceId = 0;
            long textId = 0 ;
            long sessionId = 0 ;
            long deviceId = 0;
            int numchannels = 0;
            int samplerate = 0;
            int bitdepth = 0;
            double noiselevel = 0;
            double volumelevel = 0;
            String language_name = "";
            String talent_type = "";
            String smiFileSeperator = File.separator;
            String notes = "";
            int voiceClipStatusId = 0;
            double clipPlayTime = 0;
            String shortFileName = "";
            
            try {
                voiceId = Long.parseLong(voiceIdS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter voiceId is missing.";
            }
            try{
                
                textId = Long.parseLong(textIdS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter textId is invalid type or missing.";
            }
            try{
                sessionId = Long.parseLong(sessionIdS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter sessionId is missing.";
            }
            
            try{
                numchannels = Integer.parseInt(numchannelsS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter numchannels is missing.";
            }
            try {
                samplerate = Integer.parseInt(samplerateS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter samplerate is missing.";
            }
            try {
                bitdepth = Integer.parseInt(bitdepthS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter bitdepth is missing.";
            }
            
            
            int loc =0;
            try {
                loc = Integer.valueOf(locS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter loc is missing.";
            }
            try {
                notes = (notesS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter notes is missing.";
            }
            try {
                shortFileName = (shortFileNameS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter shortFileName is missing.";
            }
            try {
                voiceClipStatusId = Integer.valueOf(voiceClipStatusIdS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter voiceClipStatusId is missing.";
            }
            try {
                clipPlayTime = Double.valueOf(clipPlayTimeS.trim());
            }
            catch (Exception e){
            //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("Error : "+e.getMessage());
                output += "The parameter clipPlayTime is missing.";
            }
            
            
            log.debug("DUMP Integers ");
            log.debug("voiceId = " + voiceId);
            log.debug("textId = " + textId);
            log.debug("sessionId = " + sessionId);
            log.debug("deviceID = " +deviceId);
            log.debug("num channels = "+numchannels);
            log.debug("samplerate = "+samplerate);
            log.debug("bitdepth = "+ bitdepth);
            log.debug("noiseleve = "+ noiselevel);
            log.debug("VolumeLevel = "+ volumelevel);
            log.debug("loc = "+ loc);
            log.debug("Now Getting Headers  ");
            
            String loginToken = null;
            int responseCode =0;
            if(output.length()==0)
            {   String myContent = "Headers are";
                if (headers != null) {
                for (String header : headers.getRequestHeaders().keySet()) {
                myContent = myContent + "Header:"+ header+" Value:"+ headers.getRequestHeader(header)+"\n";
                if (header.equalsIgnoreCase("Cookie"))
                loginToken = ""+  headers.getRequestHeader(header);
                log.debug("HeaderContent = " + myContent);
                log.debug("loginToken = " + loginToken);
                }
            }
            String uploadedFileLocation;
            String originalFileName = "";
            uploadedFileLocation = UPLOAD_LOCATION_FOLDER + fileDetail.getFileName();
            originalFileName = fileDetail.getFileName();
            log.debug("UploadedFileLocation = "+ uploadedFileLocation);
            log.debug("originalFileName = "+ originalFileName);
            // save it

            //create new file location
            Myfile myfile = new Myfile();
            boolean tokenFound;
            String result = "";
            String ovcidStr;
            UtilRedis utilRedis = new UtilRedis();
            tokenFound = utilRedis.authenticateLoginToken(loginToken);
            //tokenFound = true;
            boolean voiceFound = false; 
            if(utilRedis.is_admin_present == false){
                voiceFound = myfile.voiceIdBelongsToUser(voiceId,myfile.loginToken,myfile.JDBC_DRIVER,myfile.DB_URL,myfile.USER,myfile.PASS);
            }
            else {
                voiceFound = true;
            }
            voiceFound = true;
            //boolean sessionFound = myfile.sessionIdBelongsToUser(sessionId, loginToken,myfile.JDBC_DRIVER,myfile.DB_URL,myfile.USER,myfile.PASS);
            //boolean deviceFound = myfile.deviceIDinDB(deviceId,myfile.JDBC_DRIVER,myfile.DB_URL,myfile.USER,myfile.PASS);
            boolean textFound = myfile.textIDinDB(textId,myfile.JDBC_DRIVER,myfile.DB_URL,myfile.USER,myfile.PASS);
            if ((tokenFound == true)  && (voiceFound == true)&& (textFound == true)){
                //String pathname1 = myfile.paramMap.get("VCA_FileUploadLocation");
                String driveName = myfile.paramMap.get("VBRoot");
                log.debug("VBRoot = " + driveName);
                language_name = myfile.getLangauageName(sessionId);
                //language_name = language_name.toLowerCase();
                log.debug("language_name : " + language_name);
                talent_type = myfile.getTalentType(sessionId, voiceId)+"Voice";
                //talent_type = talent_type.toLowerCase();
                String voiceName = myfile.getVoiceName(voiceId);   //add _ instead of space 
                log.debug("voiceName : "+ voiceName);
                voiceName = voiceName.replaceAll(Pattern.quote(" "),"_");
                log.debug("voiceName : "+ voiceName);
                String pathname =  driveName + smiFileSeperator 
                        + language_name +  smiFileSeperator + talent_type + smiFileSeperator;
                pathname = pathname + voiceName + smiFileSeperator;
                log.debug("Pathname from DB = " + pathname );
                boolean isEmpty = pathname == null || pathname.trim().length() == 0;
                log.debug("Pathname is Empty " + isEmpty);
                if (isEmpty == false){
                    uploadedFileLocation = pathname +"ovc"+smiFileSeperator;
                    String ovcFileName = pathname +"ovc"+smiFileSeperator;
                    File f = new File(uploadedFileLocation);
                    if (!f.exists())
                        new File(uploadedFileLocation).mkdirs();
                        //uploadedFileLocation = uploadedFileLocation+"/ovc_"
                        //+textId+"_"+unixTime+".wav";
                        uploadedFileLocation = uploadedFileLocation + shortFileName ;
                        //ovcFileName = ovcFileName +  shortFileName";
                        // String ovcShortFileName = textId+"."+ voiceId+"."+unixTime+".ovc.wav";
                        writeToFile(uploadedInputStream, uploadedFileLocation);
                        File file =new File(uploadedFileLocation);
                        long filesz = file.length();
                        if( filesz > 0)
                        {   /*ovcidStr = myfile.createMyOVC(voiceId, deviceId, textId, sessionId, 
                            ovcFileName, samplerate, bitdepth, numchannels,
                                noiselevel,  volumelevel,  loc,  noisedataS);*/
                            String name = "";
                            name = myfile.getRecordingSessionName(sessionId);
                            String textIdStr = String.format("%05d",textId);
                            if( (name.isEmpty()) ||(name.length()== 0) || (name == null)){
                                name = ": "+ textIdStr;
                            }
                            else{
                                name = name + ": "+ textIdStr;
                            }
                            
                            log.debug("name : "+ name);
                            
                            String filenameForDB = smiFileSeperator + language_name +  smiFileSeperator + talent_type + smiFileSeperator;
                            filenameForDB = filenameForDB + voiceName + smiFileSeperator + "ovc" ;
                            filenameForDB = filenameForDB + smiFileSeperator + shortFileName;
                            log.debug(" filenameForDB : " + filenameForDB);
                            ovcidStr = myfile.createMyOVC(voiceId, textId, sessionId, filenameForDB, 
                                    samplerate, bitdepth, numchannels, loc, name, notes, 
                                    voiceClipStatusId, utilRedis.email, shortFileName, clipPlayTime);
                            try {
                                responseCode =200;
                                myObjects = new JSONObject().put("status code", responseCode).put("ID",ovcidStr);
                            } catch (JSONException ex) {
                                java.util.logging.Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            result = "" + myObjects;
                            log.debug ( "object before sending 200 Result = " + result);
                            return Response.status(responseCode).entity(result).build();
                        }
                        else{
                            String outputStr2 = "File size is 0" ;
                        try {
                            responseCode =200;
                            myObjects = new JSONObject().put("status code", responseCode).put("error message", outputStr2);
                        } catch (JSONException ex) {
                            java.util.logging.Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        result = "" + myObjects;
                        log.debug("Result = "+ result);
                         return Response.status(responseCode).entity(result).build();
                        }
                    }
                    else {
                        String outputStr2 = "File could not be uploaded" ;
                        try {
                            responseCode =200;
                            myObjects = new JSONObject().put("status code", responseCode).put("error message", outputStr2);
                        } catch (JSONException ex) {
                            java.util.logging.Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        result = "" + myObjects;
                        log.debug("Result = "+ result);
                         return Response.status(200).entity(result).build();
                        }
                }
                else{
                //String output = "Error: File could not be uploaded" ;
                //JSONObject output = null;
                    responseCode = 401;
                    String errortext= "";
                    if (tokenFound == false){
                        errortext +="unauthorized access.";
                    }
                    else if (voiceFound == false){
                        responseCode = 200;
                        errortext +="The voice id does not belong to you.";
                    }
                    else if(textFound == false){
                        responseCode = 200;
                        errortext +="The text id is not in database.";
                    }
                try {
                    
                    myObjects =new JSONObject().put("status code", responseCode).put("error message", errortext);
                } catch (JSONException ex) {
                    java.util.logging.Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
                }
                result = "" + myObjects;
                log.debug ( "object before sending 401 = " + result);
                return Response.status(responseCode).entity(result).build();
                }
            }
        else {
            responseCode = 200;
            String errortext = output;
            
            try {
                myObjects =new JSONObject().put("status code", responseCode).put("error message",  errortext);
            } catch (JSONException ex) {
                java.util.logging.Logger.getLogger(UploadFileService.class.getName()).log(Level.SEVERE, null, ex);
            }
            output =""+ myObjects;
            log.debug( "BEfore sending result back = " + output );
            return Response.status(400).entity(output).build();

        }
    }
}

	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {

		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
                        uploadedInputStream.close();
                        
		} catch (IOException e) {

			e.printStackTrace();
                        log.debug("ERROR: " +e.getLocalizedMessage());
		}

	}
}
