

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;

//import com.lambdaworks.redis.KeyValue;
//import com.lambdaworks.redis.ReadFrom;
import org.json.JSONArray;
import org.json.JSONObject;
//import com.lambdaworks.redis.RedisClient;

//import com.lambdaworks.redis.RedisClient;
//import com.lambdaworks.redis.RedisConnection;
//import com.lambdaworks.redis.RedisURI;
//import com.lambdaworks.redis.api.StatefulRedisConnection;
//import com.lambdaworks.redis.api.sync.RedisCommands;
//import com.lambdaworks.redis.codec.Utf8StringCodec;
//import com.lambdaworks.redis.masterslave.MasterSlave;
//import com.lambdaworks.redis.masterslave.StatefulRedisMasterSlaveConnection;
import java.net.InetAddress;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.json.JSONException;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 *
 * @author bharati
 */
public class UtilRedis {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UtilRedis.class);
    private static final String MASTER_NAME = "mymaster";
    private static final Set<String> sentinels;
    static {
        sentinels = new HashSet<String>();
        sentinels.add("10.1.10.16:26379");
        sentinels.add("10.1.10.18:26379");
        sentinels.add("10.1.10.19:26379");
    }

    Map<String,String> mapUser = null;
    String email = null;
    long counter = 0;
    int returnCode = -1;
    String fileName = "";
    String REDIS_WRITE_URL_ENV = "";
    String REDIS_READ_URL_ENV = "";
    String REDIS_READ2_URL_ENV = "";
    String REDIS_PASSWORD = "";
    String REDIS_PORTSTR = "";
    int REDIS_PORT = 0;
    String sayServiceName = "";
    String loginToken;
    String sessionToken;
    int permissions;
    int timeout;
    String SAY_SERVICE_SUFFIX_IP_MODE = "";
    String SAY_SERVICE_SUFFIX_MODE ="";
    String indexCurrent;
    long incrCounter;
    String queue_name = "";
    long language_id = 0;
    boolean supported = false;
    String styleMoodGestureError = "";
    long voice_owner_id = 0;
    boolean is_admin_present = false;
    String localLoginToken ="";
    
    public UtilRedis(){
        Context env;
        
        try {
            timeout = 0;
            env = (Context)new InitialContext().lookup("java:comp/env");
            REDIS_WRITE_URL_ENV = (String)env.lookup("REDIS_WRITE_URL_ENV");
            REDIS_READ_URL_ENV = (String)env.lookup("REDIS_READ_URL_ENV");
            REDIS_READ2_URL_ENV = (String)env.lookup("REDIS_READ2_URL_ENV");
            SAY_SERVICE_SUFFIX_IP_MODE = (String)env.lookup("SAY_SERVICE_SUFFIX_IP_MODE");
            REDIS_PASSWORD = (String)env.lookup("REDIS_PASSWORD");
            try{
                REDIS_PORTSTR = (String)env.lookup("REDIS_PORT");
                REDIS_PORT = Integer.parseInt(REDIS_PORTSTR);
            }
            catch(Exception e){
                log.debug("REDIS_PORTSTR : " + REDIS_PORTSTR);
                log.debug("REDIS_PORT : " + REDIS_PORT);
            } 
            try{
                SAY_SERVICE_SUFFIX_MODE = (String)env.lookup("SAY_SERVICE_SUFFIX_MODE");
            }
            catch(Exception e){
                log.debug("SAY_SERVICE_SUFFIX_MODE");
            }
            
            } 
        catch (NamingException ex) {
            log.debug("Error reading SAY_SERVICE_NAME_QA "+ ex.getMessage() +" " + ex.getLocalizedMessage());
            java.util.logging.Logger.getLogger(UtilRedis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        log.debug("1.0 sayServiceName = " + sayServiceName);
        //pool = new JedisPool(new JedisPoolConfig(),REDIS_WRITE_URL_ENV);
    }
    public String getEmail(){
        return email;
    }
    public long getVoiceOwnerId(){
        return voice_owner_id;
    }
    String createServiceName(long smi_voice_id, long language_id, String loginToken) {
        String ipStr ="";
        log.debug("SAY_SERVICE_SUFFIX_IP_MODE : " + SAY_SERVICE_SUFFIX_IP_MODE);
        if(SAY_SERVICE_SUFFIX_IP_MODE.equalsIgnoreCase("true")){
            InetAddress IP = null;
            try {
                IP = InetAddress.getLocalHost();
            } catch (Exception ex) {
                log.debug("Error"+ ex.getLocalizedMessage()+ "  "+ ex.getMessage());
            }
            if(IP != null){
                ipStr = IP.getHostAddress();
                ipStr = ipStr.replaceAll(Pattern.quote("."),"_");
                ipStr = "_"+ipStr;
            }
        }
        log.debug("Machine Ip address is = " + ipStr);
        String localServiceName =null;
        log.debug("queue_name : " + queue_name);
        if(queue_name == null){
            localServiceName = "say_language_"+language_id;
            log.debug("queue_name : " + queue_name);
        }
        else{
            localServiceName = queue_name;
        }
        localServiceName += ipStr;
        localServiceName += SAY_SERVICE_SUFFIX_MODE;
        //localServiceName = "testService";
        log.debug("localServiceName  : "+ localServiceName);
        return localServiceName;
    }
    boolean checkRedis(long id, String objCookieStr) {
        
        log.debug("Check if user exist in Redis.");
        boolean foundUser =false;
        /*RedisCommands<String, String> jedis = null;
        RedisClient redisClient = RedisClient.create();
        //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
        String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
        log.debug("Redis URL String = " + urlString);
        RedisURI uri = RedisURI.create(urlString);
        //StatefulRedisMasterSlaveConnection<String, String> connection = null;
        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        
        try{
            //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
            //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            log.debug("Connected to Redis");
            log.debug("IsMulti Value: " +connection.isMulti());
            jedis = connection.sync();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        //now Implement the logic to find the user id.
        Map<String,String> mapUser = null;
        try{
            log.debug("lets check if user id exist");
            log.debug("user:" + objCookieStr);
            mapUser = jedis.hgetAll("user:" + objCookieStr);
            log.debug("Hash Object:  " + mapUser.toString());
            log.debug("Hash Object:  " + mapUser.get("id"));
            if (mapUser != null){
                long myIdInRedis = Long.parseLong(mapUser.get("id"));
                if(id == myIdInRedis){
                    foundUser = true;
                }
            }
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        /*try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }*/
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
       return foundUser; 
    }
    
    boolean createUser(long id, JSONObject userobj, JSONArray listLanguage,  String objCookieStr){
        boolean userCreated = false;
       
        //JedisSentinelPool pool = new JedisSentinelPool(MASTER_NAME, sentinels);
        Jedis jedis = null;
        try{
            /*jedis = pool.getResource();
            log.debug("Authenticating...");
            jedis.auth("MyPassword1");
            log.debug("Connected to Redis");
            */
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception ex){
            log.debug("Error : "+ ex.getLocalizedMessage() +" "+ ex.getMessage());
        }
        
        
        //now Implement the logic to create the user id.
        String myUser = "user:"+objCookieStr;
        log.debug("----------------------------------");
        log.debug( " myUser Object = " + myUser);
        log.debug("----------------------------------");
        Iterator keysIterator = userobj.keys();
        List<String> keysList = new ArrayList<String>();
       
        while(keysIterator.hasNext()) {
            String key = (String) keysIterator.next();
            keysList.add(key);
            
        }
        for (String temp : keysList) {
            log.debug(temp);
            log.debug ("Key is = " + temp);
            String keyValue = "";
            try {
                if (temp.equalsIgnoreCase("active")){
                    keyValue = String.valueOf(userobj.getBoolean(temp));
                }
                else if(temp.equalsIgnoreCase("id")){
                    keyValue = String.valueOf(userobj.getLong(temp));
                }
                else if(temp.equalsIgnoreCase("voice_owner_id")){
                    keyValue = String.valueOf(userobj.getLong(temp));
                    
                }
                else{
                    keyValue = userobj.getString(temp);
                }
            } catch (JSONException ex) {
                Logger.getLogger(UtilRedis.class.getName()).log(Level.SEVERE, null, ex);
                log.error("Error : "+ ex.getMessage() + " "+ ex.getLocalizedMessage());
                log.debug("Error : "+ ex.getMessage() + " "+ ex.getLocalizedMessage());
            }
            jedis.hset(myUser, temp, keyValue);
            //jedis.expire(myUser, 24*60*60);//this will set to expire the object in 1 day.
            
	}
        jedis.hset(myUser, "cookie", objCookieStr);
        boolean foundMyUser = checkRedis( id, objCookieStr);
        userCreated = (foundMyUser == true)?true :false;
        jedis.expire(myUser,86400);
        
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return userCreated;
    }

    
    
    boolean removeUser(String loginToken) {
        boolean deleteUser =false;
        /*RedisCommands<String, String> jedis = null;
        RedisClient redisClient = RedisClient.create();
        //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
        String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
        log.debug("Redis URL String = " + urlString);
        RedisURI uri = RedisURI.create(urlString);
        //StatefulRedisMasterSlaveConnection<String, String> connection = null;
        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        
        try{
            //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
            //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            log.debug("Connected to Redis");
            log.debug("IsMulti Value: " +connection.isMulti());
            jedis = connection.sync();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        Jedis jedis = null;
        String localToken ="";
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        //now Implement the logic to find the user id.
        
        try{
            log.debug("LETS DELETE THE USER");
            Set<String> myKeys = new HashSet<String>();
            String myUser = "user:" +  localLoginToken;
            log.debug("myUser in Redis : "+ myUser);
            myKeys = jedis.hkeys(myUser);
            log.debug("Hash Object:  " + myKeys.toString());
            String id = jedis.hget(myUser, "id");
            long idL = Long.parseLong(id);
            
            if (myKeys.size()>0){
                for(String key:myKeys){
                  long result = jedis.hdel(myUser, key);
                  log.debug( "deleted key = " + key);
                }
            }
            if (checkRedis(idL,localToken)== false ){
                deleteUser = true;
                log.debug("user delete : "+ deleteUser);
            }
                
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        /*
        try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
       return deleteUser;
        
    }
    boolean getUserObject(String loginToken) {
       boolean foundUser =false;
       String tokenStr="";
       String[] parts = loginToken.split(";");
        if(!loginToken.contains("="))
           return foundUser;
        else parts = loginToken.split("=");
        if (parts.length != 2)
          return foundUser;
        //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
        if (parts[1]== null  )
           return foundUser;
        else if (parts[1].length()==0 )
           return foundUser;
        else tokenStr = parts[1];
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       
        parts = tokenStr.split("]");
        if (parts.length != 1)
          return foundUser; 
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
        if (parts[0]== null  )
           return foundUser;
        else if (parts[0].length()==0 )
           return foundUser;
        else tokenStr = parts[0];
       
        tokenStr = tokenStr.replace("\"", "");
        loginToken = tokenStr;
        log.debug("Token String = " + loginToken);
       /* RedisCommands<String, String> jedis = null;
        RedisClient redisClient = RedisClient.create();
        //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
        String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
        log.debug("Redis URL String = " + urlString);
        RedisURI uri = RedisURI.create(urlString);
        //StatefulRedisMasterSlaveConnection<String, String> connection = null;
        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        
        try{
            //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
            //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            log.debug("Connected to Redis");
            log.debug("IsMulti Value: " +connection.isMulti());
            jedis = connection.sync();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        //now Implement the logic to find the user id.
        
        try{
            log.debug("lets check if user id exist : user:"+loginToken);
            
            mapUser = jedis.hgetAll("user:" + loginToken);
            
            log.debug("Hash Object:  " + mapUser.toString());
            log.debug("Hash Object:  " + mapUser.get("email_address"));
            if (mapUser != null){
                foundUser = true;
                email =  mapUser.get("email_address");
                log.debug("email : "+ email);
            }
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
        /*
        try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
       return foundUser;  
    }
    
    
    

    boolean authenticateLoginToken(String loginToken) {
        
        boolean foundUser = false;
        boolean  tokeninDB = false;
        String tokenStr="";
        String[] parts;
        if(loginToken != null)
        {
            log.debug("Token is " + loginToken);
            if(!loginToken.contains("="))
               return tokeninDB;
            else parts = loginToken.split("=");
            if (parts.length != 2)
              return tokeninDB;
            //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
            if (parts[1]== null  )
               return tokeninDB;
            else if (parts[1].length()==0 )
               return tokeninDB;
            else tokenStr = parts[1];
           //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);

            parts = tokenStr.split("]");
            if (parts.length != 1)
              return tokeninDB; 
           //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
            if (parts[0]== null  )
               return tokeninDB;
            else if (parts[0].length()==0 )
               return tokeninDB;
            else tokenStr = parts[0];

            tokenStr = tokenStr.replace("\"", "");
            loginToken = tokenStr;
            localLoginToken = tokenStr;
            log.debug("Token String = " + loginToken);
           /*
            RedisCommands<String, String> jedis = null;
            RedisClient redisClient = RedisClient.create();
            //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
            String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
            log.debug("Redis URL String = " + urlString);
            RedisURI uri = RedisURI.create(urlString);
            //StatefulRedisMasterSlaveConnection<String, String> connection = null;
            StatefulRedisConnection<String, String> connection = redisClient.connect(uri);

            try{
                //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
                //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
                log.debug("Connected to Redis");
                log.debug("IsMulti Value: " +connection.isMulti());
                jedis = connection.sync();
            }
            catch(Exception e){
                log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
                log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            }
            */
            //now Implement the logic to find the user id.
            Jedis jedis = null;
            try{
                jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
                jedis.auth(REDIS_PASSWORD);
                log.debug("Connected to Redis");
            }
            catch(Exception e){
                log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            }

            try{
                log.debug("lets check if user id exist");
                mapUser = jedis.hgetAll("user:" + loginToken);
                //log.debug("Hash Object:  " + mapUser.toString());
                //log.debug("Hash Object:  " + mapUser.get("email"));
                permissions = Integer.parseInt(mapUser.get("permissions"));
                if (mapUser != null){
                    foundUser = true;
                    email =  mapUser.get("email_address");
                    String voice_owner_idS = mapUser.get("voice_owner_id");
                    String permissionStr = mapUser.get("permissions");
                    int permission = Integer.parseInt(permissionStr);
                    if((permission == 1) ||(permission == 2)){
                        is_admin_present = true;
                    }
                    voice_owner_id = Long.parseLong(voice_owner_idS);
                    log.debug("Email in Redis = " + email);
                    log.debug("voice_owner_id = " + voice_owner_id);

                }
            }
            catch(Exception e){
                log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            }

            /*
            try{
                connection.close();
                redisClient.shutdown();
            }
            */
            try{
                if(jedis != null && jedis.isConnected()){
                    jedis.close();
                    jedis = null;
                }
            }
            catch(Exception e){
                log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
                log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            }
        }
        return foundUser;
    }
    
    boolean authenticateLoginSessionToken(String myloginToken){
        log.debug("Now in the validateLoginSessionToken method");
        log.debug("login Token = "+ myloginToken);
       boolean  tokeninDB = false;
       String droptable = "drop table";
       String sessionUid;
       
       String[] parts = myloginToken.split(";");
       if (parts.length != 2)
          return tokeninDB;
       log.debug("2.login Token = "+ parts[0]);
       log.debug("2.session Token = "+ parts[1]);
       String loginStr = parts[0];
       String sessionStr = parts[1];
       if(!loginStr.contains("="))
           return tokeninDB;
       String[] loginparts = loginStr.split("=");
       if (loginparts.length != 2)
          return tokeninDB;
       if (loginparts[1] ==null)
           return tokeninDB;
       if (loginparts[1].length() == 0 )
           return tokeninDB;
       String loginTokenStr = loginparts[1];
       
       loginTokenStr = loginTokenStr.replace("\"", "");
       log.debug("loginTokenStr = " + loginTokenStr);
       if(!sessionStr.contains("="))
           return tokeninDB;
       String[] sessionparts = sessionStr.split("=");
       if (sessionparts.length != 2)
          return tokeninDB;
       if (sessionparts[1] ==null)
           return tokeninDB;
       if (sessionparts[1].length() == 0 )
           return tokeninDB;
       String sessionTokenStr = sessionparts[1];
       if(!sessionTokenStr.contains("]"))
           return tokeninDB;
       sessionparts = sessionTokenStr.split("]");
       if (sessionparts.length != 1)
          return tokeninDB;
       sessionTokenStr = sessionparts[0];
       sessionTokenStr = sessionTokenStr.replace("\"", "");
       log.debug("sessionTokenStr = " + sessionTokenStr);
       loginToken = loginTokenStr;
       sessionToken = sessionTokenStr;
       log.debug("3.login Token = "+ loginToken);
       log.debug("3.session Token = "+ sessionToken);
       boolean foundLogin = false;
       boolean foundSession = false;
       
        /*RedisCommands<String, String> jedis = null;
        RedisClient redisClient = RedisClient.create();
        //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
        String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
        log.debug("Redis URL String = " + urlString);
        RedisURI uri = RedisURI.create(urlString);
        //StatefulRedisMasterSlaveConnection<String, String> connection = null;
        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        
        try{
            //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
            //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            log.debug("Connected to Redis");
            log.debug("IsMulti Value: " +connection.isMulti());
            jedis = connection.sync();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
       */
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        //now Implement the logic to find the user id.
        
        try{
            log.debug("lets check if user id exist");
            mapUser = jedis.hgetAll("user:" + loginToken);
            log.debug("Hash Object:  " + mapUser.toString());
            log.debug("Hash Object:  " + mapUser.get("email"));
            if (mapUser != null){
                foundLogin = true;
                email =  mapUser.get("email");
            }
            String redisSessionStr = "";
            redisSessionStr = mapUser.get("session_uid");
            if(redisSessionStr.equals(sessionToken)){
                foundSession = true;
            }
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        /*try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }*/
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
       tokeninDB = (foundLogin == true) && ( foundSession == true);
       return tokeninDB;
       
   }

    boolean addSessionToUser(JSONObject obj, String loginToken) {
        boolean sessionAdded = false;
        //StatefulRedisConnection<String, String> connection = null;
        
        //RedisClient redisClient = RedisClient.create(REDIS_WRITE_URL_ENV);
        String tokenStr="";
        String[] parts;
        if(!loginToken.contains("="))
           return sessionAdded;
        else parts = loginToken.split("=");
        if (parts.length != 2)
          return sessionAdded;
        //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
        if (parts[1]== null  )
           return sessionAdded;
        else if (parts[1].length()==0 )
           return sessionAdded;
        else tokenStr = parts[1];
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       
        parts = tokenStr.split("]");
        if (parts.length != 1)
          return sessionAdded; 
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
        if (parts[0]== null  )
           return sessionAdded;
        else if (parts[0].length()==0 )
           return sessionAdded;
        else tokenStr = parts[0];
       
        tokenStr = tokenStr.replace("\"", "");
        loginToken = tokenStr;
        log.debug("Token String = " + loginToken);
        /*RedisCommands<String, String> jedis = null;
        RedisClient redisClient = RedisClient.create();
        //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
        String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
        log.debug("Redis URL String = " + urlString);
        RedisURI uri = RedisURI.create(urlString);
        //StatefulRedisMasterSlaveConnection<String, String> connection = null;
        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        
        try{
            //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
            //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            log.debug("Connected to Redis");
            log.debug("IsMulti Value: " +connection.isMulti());
            jedis = connection.sync();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        //now Implement the logic to create the user id.
        String myUser = "user:"+loginToken;
        log.debug("----------------------------------");
        log.debug( " myUser Object = " + myUser);
        log.debug("----------------------------------");
        
        String keyValue = "";
        try {
            keyValue = obj.getString("uid");
        } catch (JSONException ex) {
            Logger.getLogger(UtilRedis.class.getName()).log(Level.SEVERE, null, ex);
        }
        String temp = "session_uid";
        jedis.hset(myUser, temp, keyValue);
        boolean userFound  = getUserObject(loginToken); 
        if(userFound){
            String session_uid = mapUser.get("session_uid");
            log.debug("session_uid :" +session_uid);
            if(session_uid.equals(session_uid)){
                sessionAdded = true;
                log.debug("sessionAdded : "+ sessionAdded);
            }
        }
        /*try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }*/
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return sessionAdded ;
    }
    
    
       
    
    int getConnectionTimeout(){
        /*RedisCommands<String, String> jedis = null;
        RedisClient redisClient = RedisClient.create();
        //String urlString = "redis-sentinel://"+REDIS_WRITE_URL_ENV+","+REDIS_READ_URL_ENV+","+REDIS_READ2_URL_ENV+"/0#mymaster";
        String  urlString = "redis://"+REDIS_WRITE_URL_ENV;
        log.debug("Redis URL String = " + urlString);
        RedisURI uri = RedisURI.create(urlString);
        //StatefulRedisMasterSlaveConnection<String, String> connection = null;
        StatefulRedisConnection<String, String> connection = redisClient.connect(uri);
        
        try{
            //connection = MasterSlave.connect(redisClient, new Utf8StringCodec(),uri);
            //connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            log.debug("Connected to Redis");
            log.debug("IsMulti Value: " +connection.isMulti());
            jedis = connection.sync();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        log.debug ( "Get the connection timeout" );
        try{
            String abc = jedis.get("ConnectionTimeout");
            log.debug ("KEY timeout : VALUE "+ abc);
            if(abc == null){
                timeout = 600;
                String timeoutStr = ""+ timeout;
                jedis.set("ConnectionTimeout", timeoutStr);
                timeout = 600;
                }
            else{
                timeout = Integer.parseInt(abc);
                log.debug ("VALUE timeout :  "+ timeout);
                }
                log.debug ("VALUE timeout :  "+ timeout);
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        /*try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return timeout;
    }


    static boolean isSubsetInt(int arr1[], int arr2[], int m,
                                                   int n)
    {
        int i = 0, j = 0;
             
        if(m < n)
        return false;
         
        Arrays.sort(arr1); //sorts arr1
        Arrays.sort(arr2); // sorts arr2
 
        while( i < n && j < m )
        {
            if( arr1[j] < arr2[i] )
                j++;
            else if( arr1[j] == arr2[i] )
            {
                j++;
                i++;
            }
            else if( arr1[j] > arr2[i] )
                return false;
        }
         
        if( i < n )
            return false;
        else
            return true;
    }
    static boolean isSubsetLong(long arr1[], long arr2[], int m,
                                                   int n)
    {
        int i = 0, j = 0;
             
        if(m < n)
        return false;
         
        Arrays.sort(arr1); //sorts arr1
        Arrays.sort(arr2); // sorts arr2
 
        while( i < n && j < m )
        {
            if( arr1[j] < arr2[i] )
                j++;
            else if( arr1[j] == arr2[i] )
            {
                j++;
                i++;
            }
            else if( arr1[j] > arr2[i] )
                return false;
        }
         
        if( i < n )
            return false;
        else
            return true;
    }

    

    boolean writeF0Data(long labeling_id, JSONArray fnList,String labF0File) {
        boolean result = false;
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
        try{
            Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
            log.debug(timeStamp);
            Utils myUtils = new Utils();
            String driveName = myUtils.paramMap.get("VBRoot");
            driveName = driveName.replaceAll(Pattern.quote("\\"),"/");
            log.debug("Drive Name :" + driveName);
            
            
            //String theText = "This is a 123 dog";
            
            
            counter = jedis.incr("trainingRequestId");
            JSONObject obj = fnList.getJSONObject(0);
            String wavFilename = obj.getString("filename");
            //wavFilename = driveName + wavFilename;
            wavFilename = wavFilename.replace("\\", "/");
            String myString = "";
           
            
            myString = "{\"request\":" +counter + ",";
            myString += " \"clip\":\""+ wavFilename + "\" ";;
            myString += ", \"labFileContent\":\""+labF0File + "\"}";
            log.debug("myString : "+ myString);
            String serviceName = "directorF0Request";
            long len = jedis.rpush(serviceName, myString);
            
            if (len >= 1){
                result = true;
                log.debug("Result = " + result);
            }
            
        }
        catch (Exception ex) {
                log.debug("Error : "+ ex.getMessage() + " "+ ex.getLocalizedMessage());;
        }
        /*
        try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return result;
        
    }
    
    String getRedisData() {
        String response = "";
        String serviceName = "";
        int myTimeout = getConnectionTimeout();
        log.debug("Timeout : " + myTimeout);
        
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
        serviceName = "directorF0:"+counter;
        
        log.debug("blpop service name = "+ serviceName);
        //String serviceName = "sayResponse";
        List<String> obj = null;
        try{
            obj = jedis.blpop(myTimeout,serviceName);
        }
        catch(Exception e){
            log.debug("Error BLPOP: "+ e.getLocalizedMessage() +" "+ e.getMessage());
        }
        int result = -1;
        if(obj != null){
            String key = obj.get(0);
            String value = obj.get(1);
            log.debug("1.Value : "+ value);
            response = value;
            log.debug("Key = " + key +" value = "+ value + " Result = " + result);
            log.debug("Obj = " + obj.toString());
        }
        
        
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return response;
    }

    boolean compareClips(long labeling_id, String testClip, String refClip ,String labF0File) {
        boolean result = false;
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
        try{
            Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
            log.debug(timeStamp);
            
            
            
            //String theText = "This is a 123 dog";
            
            
            counter = jedis.incr("compareFilesRequestId");
            
            
            String myString = "";
           
            
            myString = "{\"request\":" +counter + ",";
            myString += " \"testClip\":\""+ testClip + "\" ";
            myString += " ,\"refClip\":\""+ refClip + "\" ";
            myString += ", \"labFileContent\":\""+labF0File + "\"}";
            
            JSONObject xyz = new JSONObject().put("request",counter)
                    .put("testClip", testClip)
                    .put("refClip",refClip)
                    .put("labFileContent", labF0File);
            String serviceName = "directorCompareRequest";
            long len = jedis.rpush(serviceName, xyz.toString());
            log.debug("Writing to Redis : "+ serviceName + " value :"+ xyz.toString());
            if (len >= 1){
                result = true;
                log.debug("Result = " + result);
            }
            
        }
        catch (Exception ex) {
                log.debug("Error : "+ ex.getMessage() + " "+ ex.getLocalizedMessage());;
        }
        /*
        try{
            connection.close();
            redisClient.shutdown();
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        */
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return result;
        
    }
    String getRedisCompareData() {
        String response = "";
        String serviceName = "";
        int myTimeout = getConnectionTimeout();
        log.debug("Timeout : " + myTimeout);
        
        Jedis jedis = null;
        try{
            jedis = new Jedis(REDIS_WRITE_URL_ENV, REDIS_PORT);
            jedis.auth(REDIS_PASSWORD);
            log.debug("Connected to Redis");
        }
        catch(Exception e){
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        
        serviceName = "directorCompare:"+counter;
        
        log.debug("blpop service name = "+ serviceName);
        //String serviceName = "sayResponse";
        List<String> obj = null;
        try{
            obj = jedis.blpop(myTimeout,serviceName);
        }
        catch(Exception e){
            log.debug("Error BLPOP: "+ e.getLocalizedMessage() +" "+ e.getMessage());
        }
        int result = -1;
        if(obj != null){
            String key = obj.get(0);
            String value = obj.get(1);
            log.debug("1.Value : "+ value);
            response = value;
            log.debug("Key = " + key +" value = "+ value + " Result = " + result);
            log.debug("Obj = " + obj.toString());
        }
        else{
            response = "request timed out";
        }
        
        try{
            if(jedis != null && jedis.isConnected()){
                jedis.close();
                jedis = null;
            }
        }
        catch(Exception e){
            log.error("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
            log.debug("Error : "+ e.getMessage() + " "+ e.getLocalizedMessage());
        }
        return response;
    }
    
    
    
  
    
}

