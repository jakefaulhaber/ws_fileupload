/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smi;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author bharati
 */
class Utils {
    Map<String,String> paramMap;
    Connection conn;
    Statement stmt;
    String loginToken;
    String sessionToken;
    long session_id;
    long user_id;
    int permissions;
    String email;
    String JDBC_DRIVER;  
    String DB_URL ;
    //  Database credentials
    String USER ;
    String PASS ;
    String loginTokenStr;
    JSONArray list =null;
    private static org.apache.log4j.Logger log = Logger.getLogger(Utils.class);
    
    public Utils(){
        
        Context env;
        try {
            env = (Context)new InitialContext().lookup("java:comp/env");
            DB_URL = (String)env.lookup("DB_URL");
            //log.debug("DB_URL = " + DB_URL);
            JDBC_DRIVER = (String)env.lookup("JDBC_DRIVER");
            //log.debug("JDBC_DRIVER = " + JDBC_DRIVER);
            USER = (String)env.lookup("USER");
            //log.debug("USER = " + USER);
            PASS = (String)env.lookup("PASS");
            //log.debug("PASS = " + PASS);
            
            boolean myb = getParams(JDBC_DRIVER,DB_URL,USER,PASS );
            log.debug("Got Params = " + myb);
            
            
            
        } catch (NamingException ex) {
            java.util.logging.Logger.getLogger(Utils.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
    }
    boolean getParams(String JDBC_DRIVER,String DBURL,String USER,String PASS ){
        paramMap = new HashMap<String,String>() ;
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
       // log.debug("Connecting to database with parameters = " +  JDBC_DRIVER +" , "+ DBURL+" , "+ USER+" , "+ PASS);
        conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
        log.debug("Creating statement...");
        stmt = conn.createStatement();
        String sql;
        ResultSet rs;
        String name;
        long longval;
        String textval;
        double doubleval;
        boolean booleanval;
        java.sql.Timestamp ts;
        int intval;
        int configuration_params_type_id = 0;
        sql = "SELECT * FROM configuration_params where language_id =0 and domain = 'default' ;";
        log.debug("Before excuting a query. SQL:" + sql);
        rs = stmt.executeQuery(sql);
        log.debug("Result set:" + sql);    
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
           name = rs.getString("name");
           configuration_params_type_id = rs.getInt("configuration_params_type_id");
           //log.debug("Name = " +name);
           //log.debug("configuration_params_type_id = " +configuration_params_type_id);

           switch(configuration_params_type_id){
               case 1: {
                        textval = rs.getString("val_text");
                        paramMap.put(name,textval);
                        //log.debug("textval = " +textval);
                        break;
                    }
               case 2: {
                        longval = rs.getLong("val_bigint");
                        paramMap.put(name,String.valueOf(longval));
                        //log.debug("longval = " +longval);
                        break;
                    }
               case 3: {
                        doubleval = rs.getDouble("val_numeric");
                        paramMap.put(name,String.valueOf(doubleval));
                        //log.debug("doubleval = " +doubleval);
                        break;
                    }
               case 4: {
                        booleanval = rs.getBoolean("val_bool");
                        paramMap.put(name,String.valueOf(booleanval));
                        //log.debug("booleanval = " +booleanval);
                        break;
                    }
               case 5: {
                        ts = rs.getTimestamp("val_date");
                        paramMap.put(name,ts.toString());
                        //log.debug("ts = " +ts);
                        break;
                    }
               case 6: {
                        intval = rs.getInt("val_integer");
                        paramMap.put(name,String.valueOf(intval));
                        //log.debug("intval = " +intval);
                        break;
                    }
           }
           
            
        }
        
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        boolean val;
        val = paramMap.size()>0;
    return val;
    }
    public String getEmail(){
        return email;
    }
    boolean authenticateLoginToken(String myloginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
       
       log.debug("Now in the method authenticate Token");
       boolean  tokeninDB = false;
       String tokenStr="";
       String[] parts;
       String droptable = "drop table";
       if(myloginToken == null    )
            return tokeninDB;
       if (myloginToken.length()==0 )
           return tokeninDB;
       boolean found = myloginToken.contains(droptable);
       if (found == true )
           return tokeninDB;
       if(!myloginToken.contains("="))
           return tokeninDB;
       else parts = myloginToken.split("=");
       if (parts.length != 2)
          return tokeninDB;
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       if (parts[1]== null  )
           return tokeninDB;
       else if (parts[1].length()==0 )
           return tokeninDB;
       else tokenStr = parts[1];
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       
       parts = tokenStr.split("]");
       if (parts.length != 1)
          return tokeninDB; 
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       if (parts[0]== null  )
           return tokeninDB;
       else if (parts[0].length()==0 )
           return tokeninDB;
       else tokenStr = parts[0];
       
       tokenStr = tokenStr.replace("\"", "");
       loginToken = tokenStr;
       log.debug("Authenticate Method tokenStr = " + tokenStr);
       try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT token,user_id, permissions,email_address FROM login_map where token ='"+ tokenStr + "';";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            String name = rs.getString("token");
            user_id = rs.getLong("user_id");
            permissions = rs.getInt("permissions");
            email = rs.getString("email_address");
            log.debug("token = " +name +" Email = "+ email);
            if (name.equals(tokenStr))
            {   tokeninDB =true;
                log.debug("token in DB = "+name );
                break;
            }
            else{   tokeninDB =false;
                    //log.debug("token not in DB = " );
            }
                
            
        }
            //Display values
         
         
            
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
       
       return tokeninDB;
       
    }
    boolean voiceIdBelongsToUser(long voiceId,String loginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceIdBelongsToUser");
    log.debug("Login Token = " + loginToken);
    boolean  voiceinDB = false;
    if(loginToken == null    )
            return voiceinDB;
    if (loginToken.length()==0 )
           return voiceinDB;
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select voice_id from voice where voice_owner_id = (select voice_owner_id from voice_owner where user_id =(select user_id from login_map where token='"+ loginToken + "'));";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myVoiceId = rs.getLong("voice_id");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (myVoiceId == voiceId)
            {   voiceinDB =true;
                log.debug("MyVoiceId in DB = "+myVoiceId );
                break;
            }
            else{   voiceinDB =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return voiceinDB;
}
    boolean idBelongsToUser(long userId,String loginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceOwnerIdBelongsToUser");
    log.debug("1.Login Token = "+ loginToken);
    boolean  userinDB = false;
    if(loginToken == null)
            return userinDB;
    if (loginToken.length()==0 )
           return userinDB;
    if(!loginToken.contains("="))
           return userinDB;
    String[] parts = loginToken.split("=");
    if (parts.length != 2)
          return userinDB;
    if (parts[1].length()==0 )
           return userinDB;
    String tokenStr = parts[1];
    log.debug("2.Login Token = "+ tokenStr);
    parts = tokenStr.split("]");
    if (parts.length != 1)
          return userinDB;
    log.debug("3.Login Token = "+ parts[0]);
    tokenStr = parts[0];
    tokenStr = tokenStr.replace("\"", "");
    log.debug("tokenStr = " + tokenStr);
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select user_id from login_map where token='"+ tokenStr + ");";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myUserId = rs.getLong("user_id");
            //log.debug("MyvoiceOwnerId = " + myvoiceOwnerId);
            if (myUserId == userId)
            {   userinDB =true;
                log.debug("MyUserId in DB = "+myUserId );
                break;
            }
            else{   userinDB =false;
                    //log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    
    return userinDB;
}
    
 boolean sessionIdBelongsToUser(long session_id, String myloginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method sessionIdBelongsToUser");
    log.debug("Login Token = " + myloginToken);
    boolean  sessioninDB = false;
    if(myloginToken == null    )
        return sessioninDB;
    if(myloginToken.length() ==0    )
        return sessioninDB;
    if(!myloginToken.contains(";"))
           return sessioninDB;
    String[] parts = myloginToken.split(";");
    if (parts.length != 2)
          return sessioninDB;
       String loginStr = parts[0];
       String sessionStr = parts[1];
       if(!loginStr.contains("="))
           return sessioninDB;
       String[] loginparts = loginStr.split("=");
       if (loginparts.length != 2)
          return sessioninDB;
       if (loginparts[1] ==null)
           return sessioninDB;
       if (loginparts[1].length() == 0 )
           return sessioninDB;
       String loginTokenStr = loginparts[1];
       
       loginTokenStr = loginTokenStr.replace("\"", "");
       log.debug("loginTokenStr = " + loginTokenStr);
       if(!sessionStr.contains("="))
           return sessioninDB;
       String[] sessionparts = sessionStr.split("=");
       if (sessionparts.length != 2)
          return sessioninDB;
       if (sessionparts[1] ==null)
           return sessioninDB;
       if (sessionparts[1].length() == 0 )
           return sessioninDB;
       String sessionTokenStr = sessionparts[1];
       if(!sessionStr.contains("]"))
           return sessioninDB;
       sessionparts = sessionTokenStr.split("]");
       if (sessionparts.length != 1)
          return sessioninDB;
       sessionTokenStr = sessionparts[0];
       sessionTokenStr = sessionTokenStr.replace("\"", "");
       loginToken = loginTokenStr;
       sessionToken = sessionTokenStr;
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select session_id from session where uid = '" +sessionTokenStr +"';";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long mySessionId = rs.getLong("session_id");
            //log.debug("MyvoiceOwnerId = " + mySessionId);
            if (mySessionId == session_id)
            {   sessioninDB =true;
                log.debug("MyvoiceOwnerId in DB = "+mySessionId );
                break;
            }
            else{   sessioninDB =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return sessioninDB;
}
 
boolean authenticateLoginSessionToken(String myloginToken, long voice_id, String JDBC_DRIVER, String DBURL,String USER,String PASS){
        log.debug("Now in the validateLoginSessionToken method");
        log.debug("login Token = "+ myloginToken);
       boolean  tokeninDB = false;
       String droptable = "drop table";
       String sessionUid;
       boolean found = myloginToken.contains(droptable);
       if (found == true )
           return tokeninDB;
       if(!myloginToken.contains(";")){
           log.debug("loginTokenStr = " + myloginToken + "Does not contain ';'. The header is of wrong format");
           return tokeninDB;
       }
       String[] parts = myloginToken.split(";");
       if (parts.length != 2)
          return tokeninDB;
       log.debug("2.login Token = "+ parts[0]);
       log.debug("2.session Token = "+ parts[1]);
       String loginStr = parts[0];
       String sessionStr = parts[1];
       if(!loginStr.contains("="))
           return tokeninDB;
       String[] loginparts = loginStr.split("=");
       if (loginparts.length != 2)
          return tokeninDB;
       if (loginparts[1] ==null)
           return tokeninDB;
       if (loginparts[1].length() == 0 )
           return tokeninDB;
       String loginTokenStr = loginparts[1];
       
       loginTokenStr = loginTokenStr.replace("\"", "");
       log.debug("loginTokenStr = " + loginTokenStr);
       if(!sessionStr.contains("="))
           return tokeninDB;
       String[] sessionparts = sessionStr.split("=");
       if (sessionparts.length != 2)
          return tokeninDB;
       if (sessionparts[1] ==null)
           return tokeninDB;
       if (sessionparts[1].length() == 0 )
           return tokeninDB;
       String sessionTokenStr = sessionparts[1];
       if(!sessionTokenStr.contains("]"))
           return tokeninDB;
       sessionparts = sessionTokenStr.split("]");
       if (sessionparts.length != 1)
          return tokeninDB;
       sessionTokenStr = sessionparts[0];
       sessionTokenStr = sessionTokenStr.replace("\"", "");
       log.debug("sessionTokenStr = " + sessionTokenStr);
       loginToken = loginTokenStr;
       sessionToken = sessionTokenStr;
       log.debug("3.login Token = "+ loginToken);
       log.debug("3.session Token = "+ sessionToken);
       boolean foundLogin = false;
       boolean foundSession = false;
       try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT token,user_id, permissions FROM login_map where token ='"+ loginTokenStr + "';";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            String name = rs.getString("token");
            //log.debug("token = " +name);
            user_id = rs.getLong("user_id");
            permissions = rs.getInt("permissions");
            if (name.equals(loginTokenStr))
            {   foundLogin =true;
                log.debug("token in DB = "+name );
            }
            else{   foundLogin =false;
                    //log.debug("token not in DB = " );
            }
        }
        
            sql = "SELECT uid,session_id FROM session where voice_id ='"+ voice_id + "' and uid ='"+sessionTokenStr +"';";
            log.debug("SQL:" + sql);
            ResultSet rs1 = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs1.next()){
         //Retrieve by column name
            
            String uid = rs1.getString("uid");
            //log.debug("uid = " +uid);
            sessionUid = uid;
            session_id = rs1.getLong("session_id");
            if (uid.equals(sessionTokenStr))
            {   foundSession =true;
                log.debug("session in DB = "+uid );
                break;
            }
            else{   foundSession =false;
                    //log.debug("session not in DB = " );
            }
        }
            //Display values
        //STEP 6: Clean-up environment
        rs1.close();
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
       tokeninDB = (foundLogin == true) && ( foundSession == true);
       return tokeninDB;
       
   }
boolean appIdBelongsToUser(long appId, String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceIdBelongsToUser");
    boolean  appIdinDB = false;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select application_id from application where application_id = "+ appId + ";";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myAppId = rs.getLong("application_id");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (myAppId == appId)
            {   appIdinDB =true;
                log.debug("MyAppId in DB = "+myAppId );
                break;
            }
            else{   appIdinDB =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return appIdinDB;
}

boolean languageIDinDB(long langId,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceIdBelongsToUser");
    log.debug("Login Token = " + loginToken);
    boolean  langinDB = false;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select count(*) as myCount from language where language_id = "+ langId + ";";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long count = rs.getLong("myCount");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (count>0)
            {   langinDB =true;
                log.debug("Count of language_id in DB = "+count );
                break;
            }
            else{   langinDB =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return langinDB;
}

boolean ovcIdBelongsToVoice(long voiceId,long ovcId,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method ovcIdBelongsToVoice");
    log.debug("voiceId = " + voiceId +" ovcId = " + ovcId);
    boolean  voiceInOvc = false;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select voice_id from ovc where ovc_id =" +ovcId +";";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myVoiceId = rs.getLong("voice_id");
            //log.debug("MyVoiceId = " +myVoiceId);
            if (myVoiceId == voiceId)
            {   voiceInOvc =true;
                log.debug("MyVoiceId in ovc = "+myVoiceId );
                break;
            }
            else{   voiceInOvc =false;
                   // log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return voiceInOvc;
}
    boolean authenticateVoiceEvalToken(String loginToken,String dbTokenStr)
    {
      
        boolean found = false;
        
       String tokenStr="";
       String[] parts;
       
       if(loginToken == null    )
            return found;
       if (loginToken.length()==0 )
           return found;
       if(!loginToken.contains("="))
           return found;
       else parts = loginToken.split("=");
       if (parts.length != 2)
          return found;
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       if (parts[1]== null  )
           return found;
       else if (parts[1].length()==0 )
           return found;
       else tokenStr = parts[1];
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       
       parts = tokenStr.split("]");
       if (parts.length != 1)
          return found; 
       //log.debug("Part1 0 =" + parts[0] +" and part 1 = "+ parts[1]);
       if (parts[0]== null  )
           return found;
       else if (parts[0].length()==0 )
           return found;
       else tokenStr = parts[0];
       
       tokenStr = tokenStr.replace("\"", "");
       loginToken = tokenStr;
       log.debug("Authenticate Method tokenStr = " + tokenStr);
       if (loginToken.equalsIgnoreCase(dbTokenStr))
       {
           found = true;
       }
        return found;
        
    }
    
    long getVoiceOwnerId(String loginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceIdBelongsToUser");
    log.debug("Login Token = " + loginToken);
    long  voiceOwnerId = 0;
    
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select voice_owner_id from voice_owner where user_id =(select user_id from login_map where token='"+ loginToken + "');";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            voiceOwnerId = rs.getLong("voice_owner_id");
            //log.debug("MyVoiceId = " +myVoiceId);
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    return voiceOwnerId;
}
    boolean voiceOwnerIdBelongsToUser(long voiceOwnerId,String loginToken,String JDBC_DRIVER,String DBURL,String USER,String PASS){
    log.debug("Now in the method voiceOwnerIdBelongsToUser");
    log.debug("1.Login Token = "+ loginToken);
    boolean  voiceOwnerinDB = false;
    if(loginToken == null)
            return voiceOwnerinDB;
    if (loginToken.length()==0 )
           return voiceOwnerinDB;
    if(!loginToken.contains("="))
           return voiceOwnerinDB;
    String[] parts = loginToken.split("=");
    if (parts.length != 2)
          return voiceOwnerinDB;
    if (parts[1].length()==0 )
           return voiceOwnerinDB;
    String tokenStr = parts[1];
    log.debug("2.Login Token = "+ tokenStr);
    parts = tokenStr.split("]");
    if (parts.length != 1)
          return voiceOwnerinDB;
    log.debug("3.Login Token = "+ parts[0]);
    tokenStr = parts[0];
    tokenStr = tokenStr.replace("\"", "");
    log.debug("tokenStr = " + tokenStr);
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "select voice_owner_id from voice_owner where user_id =(select user_id from login_map where token='"+ tokenStr + "');";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myvoiceOwnerId = rs.getLong("voice_owner_id");
            //log.debug("MyvoiceOwnerId = " + myvoiceOwnerId);
            if (myvoiceOwnerId == voiceOwnerId)
            {   voiceOwnerinDB =true;
                log.debug("MyvoiceOwnerId in DB = "+myvoiceOwnerId );
                break;
            }
            else{   voiceOwnerinDB =false;
                    //log.debug("token not in DB = " );
            }
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    
    return voiceOwnerinDB;
}
    
    
    boolean checkDomainPermission(long smi_domain_id, String email, String JDBC_DRIVER,String DBURL,String USER,String PASS){
    
    boolean hasPermission = false;
    try{
        //STEP 2: Register JDBC driver
           log.debug("registering class");
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DBURL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql = "";
            //sql = "select user_id from login_map where token='"+ tokenStr + ");";
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
        while(rs.next()){
         //Retrieve by column name
            
            long myUserId = rs.getLong("user_id");
            //log.debug("MyvoiceOwnerId = " + myvoiceOwnerId);
            /*if (myUserId == userId)
            {   userinDB =true;
                log.debug("MyUserId in DB = "+myUserId );
                break;
            }
            else{   userinDB =false;
                    //log.debug("token not in DB = " );
            }
                    */
                
            
        }
        //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
    
    
    return hasPermission;
}
    boolean setDirectorLoggedout(long trss_id) {
        boolean directorPresentSet = false;
        int count =0;
         try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            
        //update ovc table
            sql = "update talent_recording_session_state set is_director_present = false"; 
            sql += " where talent_recording_session_state_id = " + trss_id ;
            log.debug("SQL:" + sql);
            count = stmt.executeUpdate(sql);
            log.debug("Count of training_sequence_id updated = "+count);
            directorPresentSet = (count>0)?true:false;
            stmt.close();
            conn.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
            }catch(Exception e){
             //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("ERROR " + e.getLocalizedMessage());
            }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                 log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        
        return  directorPresentSet;
    }
    boolean setDirectorPresent(long talent_recording_session_id, long training_sequence_id, long id) {
        boolean directorPresentSet = false;
        int count =0;
         try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            
        //update ovc table
            sql = "update talent_recording_session_state set is_director_present = true,";
            sql += " director_voice_owner_id = "+ id;
            sql += " where talent_recording_session_id = " + talent_recording_session_id ;
            sql += " and training_sequence_id = " + training_sequence_id ;
            log.debug("SQL:" + sql);
            count = stmt.executeUpdate(sql);
            log.debug("Count of training_sequence_id updated = "+count);
            directorPresentSet = (count>0)?true:false;
            stmt.close();
            conn.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
            }catch(Exception e){
             //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("ERROR " + e.getLocalizedMessage());
            }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                 log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        
        return  directorPresentSet;
    }

    boolean updateTalentRecordingSession(long talent_recording_session_id, String comment) {
    boolean updated = false;
        int count =0;
         try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            
        //update ovc table
            sql = "update talent_recording_session set comments = '" + comment; 
            sql += "' where talent_recording_session_id = " + talent_recording_session_id ;
            log.debug("SQL:" + sql);
            count = stmt.executeUpdate(sql);
            log.debug("Count of training_sequence_id updated = "+count);
            updated = (count>0)?true:false;
            stmt.close();
            conn.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
            }catch(Exception e){
             //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("ERROR " + e.getLocalizedMessage());
            }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                 log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
      return updated;  
    }

    boolean referenceVoice(long voice_id, String loginToken, String JDBC_DRIVER, String DB_URL, String USER, String PASS) {
    list = new JSONArray();
        try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            
            sql = "select distinct  A.voice_owner_id,  B.type, B.voice_id as reference_voice_id, B.name" ;
            sql += " from voice_owner A, voice B   where A.voice_owner_id = B.voice_owner_id ";
            sql += " and A.can_be_reference = true ";
            sql += " and B.voice_id = " +voice_id;
            log.debug("SQL:" + sql);
            ResultSet rs = stmt.executeQuery(sql);
            
        //STEP 5: Extract data from result set
            
        while(rs.next()){
         //Retrieve by column name
            String name = rs.getString("name");
            long reference_voice_id = rs.getLong("reference_voice_id");
            String type = rs.getString("type");
            JSONObject objTName = new JSONObject().put("name",name )
                    .put("reference_voice_id",""+reference_voice_id)
                    .put("type", type);
            log.debug(objTName.toString());
            list.put(objTName);
        }
         //STEP 6: Clean-up environment
        rs.close();
        stmt.close();
        conn.close();
        }catch(SQLException se){
        //Handle errors for JDBC
            se.printStackTrace();
            log.debug("ERROR " + se.getLocalizedMessage());
        }catch(Exception e){
             //Handle errors for Class.forName
            e.printStackTrace();
            log.debug("ERROR " + e.getLocalizedMessage());
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
   boolean result = (list.length()>0)?true:false;
   return result;
    }

    boolean directorMoved( long id) {
    
        boolean directorMoved = false;
        int count =0;
         try{
        //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

        //STEP 3: Open a connection
            log.debug("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

        //STEP 4: Execute a query
            log.debug("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            
        //update ovc table
            sql = "update talent_recording_session_state set is_director_present = false";
            sql += " where director_voice_owner_id = "+ id;
            
            log.debug("SQL:" + sql);
            count = stmt.executeUpdate(sql);
            log.debug("Count of training_sequence_id updated = "+count);
            directorMoved = (count>0)?true:false;
            stmt.close();
            conn.close();
            }catch(SQLException se){
            //Handle errors for JDBC
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
            }catch(Exception e){
             //Handle errors for Class.forName
                e.printStackTrace();
                log.debug("ERROR " + e.getLocalizedMessage());
            }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                stmt.close();
            }catch(SQLException se2){
                 log.debug("ERROR " + se2.getLocalizedMessage());
                }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
                log.debug("ERROR " + se.getLocalizedMessage());
        
            }//end finally try
        }//end try
        
        return  directorMoved;
    }
    
}


